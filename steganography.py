'''
This is a script which embeds and can decode messages in the alpha channel of a 4-channel PNG image.
The base image should only have 3 channels (i.e. RGB only). Any other will be ignored.
Strings are converted to a binary representation in utf-32 and encoded via vigenere cipher

There is a reason this was made public.

Tested with python3.7.

kjalaric@gitlab
'''

import math
import cv2
import numpy as np

ALPHA_0 = 255
ALPHA_1 = 254
EOM = 253
ENCODING_CHAR_SIZE = 32
ENCODING_MAX_VALUE = int(math.pow(2, 32))


def alpha_to_binary(alpha_value):
    if alpha_value == ALPHA_0:
        return '0'
    elif alpha_value == ALPHA_1:
        return '1'


def vigenere_cipher(deciphered_string, key):
    encoded_characters = list()
    keylength = len(key)
    for i, v in enumerate(deciphered_string):
        key_c = key[i % keylength]
        encoded_c = chr(ord(v) + ord(key_c) % ENCODING_MAX_VALUE)
        encoded_characters.append(encoded_c)
    return "".join(encoded_characters)


def vigenere_decipher(ciphered_string, key):
    encoded_characters = list()
    keylength = len(key)
    for i, v in enumerate(ciphered_string):
        key_c = key[i % keylength]
        encoded_c = chr(ord(v) - ord(key_c) % ENCODING_MAX_VALUE)
        encoded_characters.append(encoded_c)
    return "".join(encoded_characters)


def encrypt(image_location, output_location, message, key):
    imarr = cv2.imread(image_location)
    imshape = imarr.shape

    if not imshape[2] == 3:
        print("Warning: image doesn't have three channels; alpha channel will likely be overwritten.")

    if (len(test_string)*8 + 1) > (imshape[0] * imshape[1]):
        raise Exception("Image isn't big enough to contain the specified message.")

    messagearr = np.ones(imshape[0] * imshape[1], dtype=np.uint8) * 255

    binary_string = vigenere_cipher(message, key).encode('utf-32')
    for ii, vv in enumerate(binary_string):
        for i, v in enumerate('{0:08b}'.format(vv)):
            if v == '0':
                messagearr[ii*8 + i] = ALPHA_0
            elif v == '1':
                messagearr[ii*8 + i] = ALPHA_1
    messagearr[len(binary_string)*8] = EOM
    messagearr = np.reshape(messagearr, (imshape[0], imshape[1]))

    imout = np.zeros(shape=(imshape[0], imshape[1], 4))
    imout[:,:,0] = imarr[:,:,0]
    imout[:,:,1] = imarr[:,:,1]
    imout[:,:,2] = imarr[:,:,2]
    imout[:,:,3] = messagearr

    cv2.imwrite(output_location, imout)
    print(f"Image {output_location} generated. Used {(len(binary_string)*8+1)/(imshape[0]*imshape[1])}% of the available image space.")


def decrypt(image_location, key):
    imarr = cv2.imread(image_location, cv2.IMREAD_UNCHANGED)
    imshape = imarr.shape

    if not imshape[2] == 4:
        raise Exception("Image doesn't have an alpha channel.")

    alphachan = list(imarr[:,:,3].flatten())
    encoded_string = "".join(alpha_to_binary(x) for x in alphachan[0:alphachan.index(EOM)])

    decoded_string = ''

    for i in range(1, len(encoded_string) // ENCODING_CHAR_SIZE + 1):
        # print(encoded_string[(i-1)*ASCII_CHAR_SIZE:i*ASCII_CHAR_SIZE])
        try:
            decoded_string += int(encoded_string[(i-1) * ENCODING_CHAR_SIZE:i * ENCODING_CHAR_SIZE], 2).to_bytes(ENCODING_CHAR_SIZE // 8, 'big').decode('utf-32')
        except Exception as e:
            # print(e)
            pass

    return(vigenere_decipher(decoded_string, key))


if __name__ == "__main__":
    # unit tests
    test_image = "base_image.png"
    output_image = "encrypted_image.png"
    test_string = "This is a script which embeds messages in the alpha channel of a PNG image."

    # cipher
    print (test_string == vigenere_decipher(vigenere_cipher(test_string, "cheese"), "cheese"))

    # image embed and decrypt - ciphered
    encrypt(test_image, output_image, test_string, "jellyfish")
    print(decrypt(output_image, "bamboozled") != test_string)
    print(decrypt(output_image, "jellyfish") == test_string)