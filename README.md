# steganography

Scripts which can be used for hiding messages in plain sight.

# steganography.py
This script involves taking an input image file location, message string and key string.
* the message is encrypted via Vigenere cipher based on the key
* the encrypted message is stored in the alpha channel of the output image
* the output image will have the same appearance as the input image
* the output image can then be decoded (provided the correct key is supplied) and original message will be provided

So, for example, I could hide a message in an image and give the key to someone, then whoever has both the key and image containing the message could extract the message in plaintext.

Sample use:
```python
import steganography

# encryption
steganography.encrypt("input_image.png", "image_with_ciphered_message.png", "This is a secret message", "swordfish")

# decryption
steganography.decrypt("image_with_ciphered_message.png", "swordfish")
```